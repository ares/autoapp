// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build !windows

package autoapp

import (
	"os"

	"github.com/kless/goutil"
)

const ROOT = "/"

var DIR_HOME string

func init() {
	DIR_HOME = os.Getenv("HOME")
	if DIR_HOME == "" {
		goutil.Fatalln("environment variable is not set: $HOME")
	}
	DIR_HOME += "/." + DIR_APP
}

// _EXT_BIN is the extension for an executable binary.
const _EXT_BIN = ""

// GetAppRoot returns the absolute path of apps directory.
//func GetAppRoot() string { return ROOT + DIR_APP }
