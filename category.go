// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

// == Licenses

type LicenseType int

const (
	// == Licenses for Open Source Software
	APACHE_2_0 = iota
	GPL_V3
	GPL_V2
	AGPL_V3
	MIT

	ARTISTIC_2_0
	EPL_1_0
	MPL_2_0
	//LGPL_V3_0
	//LGPL_V2_1

	BSD
	BSD_3
	ISC

	UNLICENSE
	CC0_1_0

	// Specific license for commercial software
	COMMERCIAL
)

var Licenses = [...]string{
	APACHE_2_0: "APACHE_2_0",
	GPL_V3:     "GPL_V3",
	GPL_V2:     "GPL_V2",
	AGPL_V3:    "AGPL_V3",
	MIT:        "MIT",

	ARTISTIC_2_0: "ARTISTIC_2_0",
	EPL_1_0:      "EPL_1_0",
	MPL_2_0:      "MPL_2_0",
	//LGPL_V3_0: "LGPL_V3_0",
	//LGPL_V2_1: "LGPL_V2_1",

	BSD:   "BSD",
	BSD_3: "BSD_3",
	ISC:   "ISC",

	UNLICENSE: "UNLICENSE",
	CC0_1_0:   "CC0_1_0",

	COMMERCIAL: "COMMERCIAL",
}

var Licenses_string = [...]string{
	APACHE_2_0: "Apache License 2.0",
	GPL_V3:     "GNU GPL v3.0",
	GPL_V2:     "GNU GPL v2.0",
	AGPL_V3:    "GNU Affero GPL v3.0",
	MIT:        "MIT License",

	ARTISTIC_2_0: "Artistic License 2.0",
	EPL_1_0:      "Eclipse Public License v1.0",
	MPL_2_0:      "Mozilla Public License 2.0",
	//LGPL_V3_0: "GNU LGPL v3.0",
	//LGPL_V2_1: "GNU LGPL v2.1",

	BSD:   "Simplified BSD",
	BSD_3: "New BSD",
	ISC:   "ISC",

	UNLICENSE: "Public Domain (Unlicense)",
	CC0_1_0:   "CC0 1.0 Universal",

	COMMERCIAL: "commercial",
}

var Licenses_address = [...]string{
	"http://choosealicense.com/licenses/apache-2.0/",
	"http://choosealicense.com/licenses/gpl-3.0/",
	"http://choosealicense.com/licenses/gpl-2.0/",
	"http://choosealicense.com/licenses/agpl-3.0/",
	"http://choosealicense.com/licenses/mit/",

	"http://choosealicense.com/licenses/artistic-2.0/",
	"http://choosealicense.com/licenses/epl-1.0/",
	"http://choosealicense.com/licenses/mpl-2.0/",
	//"http://choosealicense.com/licenses/lgpl-3.0/",
	//"http://choosealicense.com/licenses/lgpl-2.1/",

	"http://choosealicense.com/licenses/bsd-2-clause/",
	"http://choosealicense.com/licenses/bsd-3-clause/",
	"http://choosealicense.com/licenses/isc/",

	"http://choosealicense.com/licenses/unlicense/",
	"http://choosealicense.com/licenses/cc0/",

	"",
}

// == App categories
//

// Based in categories of Android Apps: https://play.google.com/store/apps

// Identifiers of categories
const (
	BOOKS_AND_REFERENCE = iota
	BUSINESS
	COMICS
	COMMUNICATION
	EDUCATION
	ENTERTAINMENT
	FINANCE
	HEALTH_AND_FITNESS
	LIBRARIES_AND_DEMO
	LIFESTYLE
	APP_WALLPAPER
	MEDIA_AND_VIDEO
	MEDICAL
	MUSIC_AND_AUDIO
	NEWS_AND_MAGAZINES
	PERSONALIZATION
	PHOTOGRAPHY
	PRODUCTIVITY
	SHOPPING
	SOCIAL
	SPORTS
	TOOLS
	TRANSPORTATION
	TRAVEL_AND_LOCAL
	WEATHER
	APP_WIDGETS
	GAME
)

// Identifiers of games subcategories
const (
	GAME_ACTION = iota
	GAME_ADVENTURE
	GAME_ARCADE
	GAME_BOARD
	GAME_CARD
	GAME_CASINO
	GAME_CASUAL
	GAME_EDUCATIONAL
	GAME_FAMILY
	GAME_WALLPAPER
	GAME_MUSIC
	GAME_PUZZLE
	GAME_RACING
	GAME_ROLE_PLAYING
	GAME_SIMULATION
	GAME_SPORTS
	GAME_STRATEGY
	GAME_TRIVIA
	GAME_WIDGETS
	GAME_WORD
)

var Categories = [...]string{
	BOOKS_AND_REFERENCE: "BOOKS_AND_REFERENCE",
	BUSINESS:            "BUSINESS",
	COMICS:              "COMICS",
	COMMUNICATION:       "COMMUNICATION",
	EDUCATION:           "EDUCATION",
	ENTERTAINMENT:       "ENTERTAINMENT",
	FINANCE:             "FINANCE",
	HEALTH_AND_FITNESS:  "HEALTH_AND_FITNESS",
	LIBRARIES_AND_DEMO:  "LIBRARIES_AND_DEMO",
	LIFESTYLE:           "LIFESTYLE",
	APP_WALLPAPER:       "APP_WALLPAPER",
	MEDIA_AND_VIDEO:     "MEDIA_AND_VIDEO",
	MEDICAL:             "MEDICAL",
	MUSIC_AND_AUDIO:     "MUSIC_AND_AUDIO",
	NEWS_AND_MAGAZINES:  "NEWS_AND_MAGAZINES",
	PERSONALIZATION:     "PERSONALIZATION",
	PHOTOGRAPHY:         "PHOTOGRAPHY",
	PRODUCTIVITY:        "PRODUCTIVITY",
	SHOPPING:            "SHOPPING",
	SOCIAL:              "SOCIAL",
	SPORTS:              "SPORTS",
	TOOLS:               "TOOLS",
	TRANSPORTATION:      "TRANSPORTATION",
	TRAVEL_AND_LOCAL:    "TRAVEL_AND_LOCAL",
	WEATHER:             "WEATHER",
	APP_WIDGETS:         "APP_WIDGETS",
	GAME:                "GAME",
}

var SubcategoriesGames = [...]string{
	GAME_ACTION:       "GAME_ACTION",
	GAME_ADVENTURE:    "GAME_ADVENTURE",
	GAME_ARCADE:       "GAME_ARCADE",
	GAME_BOARD:        "GAME_BOARD",
	GAME_CARD:         "GAME_CARD",
	GAME_CASINO:       "GAME_CASINO",
	GAME_CASUAL:       "GAME_CASUAL",
	GAME_EDUCATIONAL:  "GAME_EDUCATIONAL",
	GAME_FAMILY:       "GAME_FAMILY",
	GAME_WALLPAPER:    "GAME_WALLPAPER",
	GAME_MUSIC:        "GAME_MUSIC",
	GAME_PUZZLE:       "GAME_PUZZLE",
	GAME_RACING:       "GAME_RACING",
	GAME_ROLE_PLAYING: "GAME_ROLE_PLAYING",
	GAME_SIMULATION:   "GAME_SIMULATION",
	GAME_SPORTS:       "GAME_SPORTS",
	GAME_STRATEGY:     "GAME_STRATEGY",
	GAME_TRIVIA:       "GAME_TRIVIA",
	GAME_WIDGETS:      "GAME_WIDGETS",
	GAME_WORD:         "GAME_WORD",
}

// * * *

// English, by default

var Categories_en = [...]string{
	BOOKS_AND_REFERENCE: "Books & Reference",
	BUSINESS:            "Business",
	COMICS:              "Comics",
	COMMUNICATION:       "Communication",
	EDUCATION:           "Education",
	ENTERTAINMENT:       "Entertainment",
	FINANCE:             "Finance",
	HEALTH_AND_FITNESS:  "Health & Fitness",
	LIBRARIES_AND_DEMO:  "Libraries & Demo",
	LIFESTYLE:           "Lifestyle",
	APP_WALLPAPER:       "Wallpaper",
	MEDIA_AND_VIDEO:     "Media & Video",
	MEDICAL:             "Medical",
	MUSIC_AND_AUDIO:     "Music & Audio",
	NEWS_AND_MAGAZINES:  "News & Magazines",
	PERSONALIZATION:     "Personalization",
	PHOTOGRAPHY:         "Photography",
	PRODUCTIVITY:        "Productivity",
	SHOPPING:            "Shopping",
	SOCIAL:              "Social",
	SPORTS:              "Sports",
	TOOLS:               "Tools",
	TRANSPORTATION:      "Transportation",
	TRAVEL_AND_LOCAL:    "Travel & Local",
	WEATHER:             "Weather",
	APP_WIDGETS:         "Widgets",
	GAME:                "Game",
}

var SubcategoriesGames_en = [...]string{
	GAME_ACTION:       "Action",
	GAME_ADVENTURE:    "Adventure",
	GAME_ARCADE:       "Arcade",
	GAME_BOARD:        "Board",
	GAME_CARD:         "Card",
	GAME_CASINO:       "Casino",
	GAME_CASUAL:       "Casual",
	GAME_EDUCATIONAL:  "Educational",
	GAME_FAMILY:       "Family",
	GAME_WALLPAPER:    "Wallpaper",
	GAME_MUSIC:        "Music",
	GAME_PUZZLE:       "Puzzle",
	GAME_RACING:       "Racing",
	GAME_ROLE_PLAYING: "Role Playing",
	GAME_SIMULATION:   "Simulation",
	GAME_SPORTS:       "Sports",
	GAME_STRATEGY:     "Strategy",
	GAME_TRIVIA:       "Trivia",
	GAME_WIDGETS:      "Widgets",
	GAME_WORD:         "Word",
}

// Chinese
/*
var Categories_cn = [...]string{
	BOOKS_AND_REFERENCE: "",
	BUSINESS:            "",
	COMICS:              "",
	COMMUNICATION:       "",
	EDUCATION:           "",
	ENTERTAINMENT:       "",
	FINANCE:             "",
	HEALTH_AND_FITNESS:  "",
	LIBRARIES_AND_DEMO:  "",
	LIFESTYLE:           "",
	APP_WALLPAPER:       "",
	MEDIA_AND_VIDEO:     "",
	MEDICAL:             "",
	MUSIC_AND_AUDIO:     "",
	NEWS_AND_MAGAZINES:  "",
	PERSONALIZATION:     "",
	PHOTOGRAPHY:         "",
	PRODUCTIVITY:        "",
	SHOPPING:            "",
	SOCIAL:              "",
	SPORTS:              "",
	TOOLS:               "",
	TRANSPORTATION:      "",
	TRAVEL_AND_LOCAL:    "",
	WEATHER:             "",
	APP_WIDGETS:         "",
	GAME:                "",
}

var SubcategoriesGames_cn = [...]string{
	GAME_ACTION:       "",
	GAME_ADVENTURE:    "",
	GAME_ARCADE:       "",
	GAME_BOARD:        "",
	GAME_CARD:         "",
	GAME_CASINO:       "",
	GAME_CASUAL:       "",
	GAME_EDUCATIONAL:  "",
	GAME_FAMILY:       "",
	GAME_WALLPAPER:    "",
	GAME_MUSIC:        "",
	GAME_PUZZLE:       "",
	GAME_RACING:       "",
	GAME_ROLE_PLAYING: "",
	GAME_SIMULATION:   "",
	GAME_SPORTS:       "",
	GAME_STRATEGY:     "",
	GAME_TRIVIA:       "",
	GAME_WIDGETS:      "",
	GAME_WORD:         "",
}

// Spanish

// Japanese

// Portuguese

// German

// Arabic

// French

// Russian

// Korean

*/
