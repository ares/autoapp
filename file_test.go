// Copyright 2014 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import "fmt"

func ExampleAppFiles() {
	appName := NewAppName(APP_NAME, APP_VERSION)

	appFiles := NewAppFiles(appName)
	userFiles := NewUserFiles(appName)
	fmt.Println(appFiles)
	fmt.Println(userFiles)

	// Output:
	// &{/app/Apps/foo/1.0.0 CHANGELOG.md README.md spec.yaml bin conf data doc share tool web }
	// &{/app/Users /app/Users/conf/foo/1.0.0.cfg /app/Users/data/foo/1.0.0 /app/Users/log/foo/1.0.0.log}
}
