// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
)

var Verbose *bool

// AppName represents the full name of an app.
type AppName struct {
	name    string
	version string
	os      string
	arch    string

	name_version string
}

// NewAppName returns an AppName with both operating system and architecture
// for the current system.
func NewAppName(name, version string) *AppName {
	return &AppName{
		name:    name,
		version: version,
		os:      runtime.GOOS,
		arch:    runtime.GOARCH,

		name_version: name + "-" + version,
	}
}

// SetSystem sets both operating system and architecture.
func (a *AppName) SetSystem(os, arch string) error {
	_, found := validOS[os]
	if !found {
		return fmt.Errorf("unsopported operating system: %s", os)
	}
	if _, found = validArch[arch]; !found {
		return fmt.Errorf("unsopported architecture: %s", arch)
	}

	a.os, a.arch = os, arch
	return nil
}

// Checking checks that the app name and version are right.
func (a *AppName) Checking() error {
	err := ValidateAppName(a.name)
	if err != nil {
		return err
	}
	if err = ValidateVersion(a.version); err != nil {
		return err
	}

	return nil
}

// FullName returns the full name of an app.
func (a *AppName) FullName() string {
	return fmt.Sprintf("%s-%s-%s-%s", a.name, a.version, a.os, a.arch)
}

func (a *AppName) Name() string {
	return fmt.Sprintf("%s-%s%s", a.name, a.version, EXT_APP)
}

// GetAppName slices "fullName" into all substrings separated by "-" and
// returns an "AppName" with the substrings between those separators.
func GetAppName(fullName string) (*AppName, error) {
	names := strings.Split(fullName, "-")
	if len(names) < 2 {
		return nil, fmt.Errorf(`at least should have two components separated by "-"`)
	}
	if len(names) > 4 {
		return nil, fmt.Errorf(`as maximum should have four components separated by "-"`)
	}
	if len(names) == 3 {
		return nil, fmt.Errorf(`could not have three components separated by "-"`)
	}

	appName := NewAppName(names[0], names[1])
	err := appName.Checking()
	if err != nil {
		return nil, err
	}
	if len(names) == 4 {
		if err = appName.SetSystem(names[2], names[3]); err != nil {
			return nil, err
		}
	}

	return appName, nil
}

// == Errors

// NoDirError reports when a file is not a directory.
type NoDirError string

func (e NoDirError) Error() string {
	return "no directory: " + string(e)
}

// NoFileError reports when a file is not regular.
type NoFileError string

func (e NoFileError) Error() string {
	return "no regular file: " + string(e)
}

// ExtError reports incorrect extension for an app.
type ExtError string

func (e ExtError) Error() string {
	return "wrong extension for an app: " + filepath.Base(string(e))
}

// A NoConfigError reports the absence of configuration file.
type NoConfigError string

func (e NoConfigError) Error() string {
	return "app has not configuration file: " + filepath.Base(string(e))
}
