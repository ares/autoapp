// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"errors"
	"fmt"
	"io"
	"net/mail"
	"net/url"
	"reflect"
	"strings"
	"text/scanner"

	"github.com/blang/semver"
	"github.com/kless/goutil/ascii"
)

// Config represents the configuration of an "autoapp" package.
type Config struct {
	Name        string // Required
	Version     string // Required
	Description string // Required
	Author      string
	Authors     []string
	Homepage    string

	License     string // Required
	LicenseText string // For commercial licenses

	// TODO: free, paid app

	Category string // Required // TODO: mayve Section ???
	Game     string // Subcategory
}

var mulAuthorErr = errors.New(`both "author" and "authors" fields can not be set`)

// Check checks that the configuration have all required fields and validate
// the values.
func (cfg Config) Check() error {
	structVal := reflect.ValueOf(cfg)
	structType := reflect.TypeOf(cfg)
	emptyFields := make([]string, 0)

	var hasAuthor, hasAuthors bool

	for i := 0; i < structVal.NumField(); i++ {
		fieldName := structType.Field(i).Name

		switch fieldName {
		// Required fields
		case "Name", "Version", "Description", "License", "Category":
			if structVal.Field(i).String() == "" {
				emptyFields = append(emptyFields, strings.ToLower(fieldName))
			}

		case "Author":
			if structVal.Field(i).String() != "" {
				hasAuthor = true
			}
		case "Authors":
			if structVal.Field(i).Len() != 0 {
				hasAuthors = true
			}
		}
	}

	if len(emptyFields) != 0 {
		field := "field"
		if len(emptyFields) != 1 {
			field += "s"
		}

		return fmt.Errorf("'%s': configuration file with empty or not found %s:\n\t%v", f_SPEC, field, emptyFields)
	}
	if hasAuthor && hasAuthors {
		return mulAuthorErr
	}

	// == Validate fields

	var err error

	// Required
	if err = ValidateAppName(cfg.Name); err != nil {
		return err
	}
	if err = ValidateVersion(cfg.Version); err != nil {
		return err
	}
	if err = ValidateLicense(cfg.License); err != nil {
		return err
	}
	if err = ValidateCategory(cfg.Category); err != nil {
		return err
	}

	if hasAuthor {
		if err = ValidateAuthor(cfg.Author); err != nil {
			return err
		}
	}
	if hasAuthors {
		for _, v := range cfg.Authors {
			if err = ValidateAuthor(v); err != nil {
				return err
			}
		}
	}

	if cfg.Homepage != "" {
		if err = ValidateHomepage(cfg.Homepage); err != nil {
			return err
		}
	}
	if cfg.Game != "" {
		if err = ValidateCategoryGame(cfg.Game); err != nil {
			return err
		}
	}

	if cfg.License == "COMMERCIAL" && cfg.LicenseText == "" {
		return fmt.Errorf("text of commercial license is empty")
	}

	return nil
}

// == Validations
//

// ValidateAuthor checks whether the email is valid.
func ValidateAuthor(s string) error {
	if strings.ContainsAny(s, "@.<>") {
		if _, err := mail.ParseAddress(s); err != nil {
			return fmt.Errorf("invalid program author: %s", err)
		}

		// Check TLD
		domain := strings.SplitAfter(s, "@")[1]
		if !strings.ContainsRune(domain, '.') {
			return domainError(domain)
		}
	}

	return nil
}

// ValidateCategory checks that the category is valid.
func ValidateCategory(s string) error {
	for _, cat := range Categories {
		if s == cat {
			return nil
		}
	}
	return fmt.Errorf("invalid category: %s", s)
}

// ValidateCategoryGame checks that the subcategory of game is valid.
func ValidateCategoryGame(s string) error {
	for _, cat := range SubcategoriesGames {
		if s == cat {
			return nil
		}
	}
	return fmt.Errorf("invalid games subcategory: %s", s)
}

// ValidateHomepage checks whether the home page is an URL valid.
func ValidateHomepage(s string) error {
	url_, err := url.Parse(s)
	if err != nil {
		return homepageError(err.Error())
	}

	if !url_.IsAbs() {
		return homepageError("URL is not absoulte: " + s)
	}
	// Check TLD
	if !strings.ContainsRune(s, '.') {
		return domainError(s)
	}

	return nil
}

// ValidateLicense checks whether the license is valid.
func ValidateLicense(s string) error {
	for _, lic := range Licenses {
		if s == lic {
			return nil
		}
	}
	return fmt.Errorf("invalid license: %s", s)
}

// ValidateAppName checks the app name.
//
// It should be all lowercase, with underscores to separate words,
// just_like_this. Stick with basic Latin letters and Arabic digits: [a-z0-9_]
// and ensure that it’s a valid Go identifier (i.e. doesn’t start with digits
// and isn’t a reserved word).
func ValidateAppName(n string) error {
	var s scanner.Scanner
	s.Init(strings.NewReader(n))

	tok := s.Scan()
	if tok != scanner.Ident {
		return nameError(n)
	}
	// Could have a white space, i.e. "foo bar"
	if tok = s.Scan(); tok != scanner.EOF {
		return nameError(n)
	}

	// The valid indentifier for Go could have an Unicode character.
	rd := strings.NewReader(n)
	for {
		char, err := rd.ReadByte()
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}

		if !ascii.IsLetter(char) && !ascii.IsDigit(char) && char != '_' {
			return nameError(n)
		}
	}

	return nil
}

// ValidateVersion checks that the version follows semantic versioning
// (http://semver.org/).
func ValidateVersion(s string) error {
	ver, err := semver.Parse(s)
	if err != nil {
		return versionError(err.Error())
	}
	if err = ver.Validate(); err != nil {
		return versionError(err.Error())
	}

	return nil
}

// * * *

type domainError string

func (e domainError) Error() string {
	return "no top-level domain: " + string(e)
}

type homepageError string

func (e homepageError) Error() string {
	return "invalid program homepage: " + string(e)
}

type nameError string

func (e nameError) Error() string {
	return `invalid program name: "` + string(e) + `"`
}

type versionError string

func (e versionError) Error() string {
	return "invalid program version: " + string(e)
}
