// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	//"strings"

	"github.com/kless/goutil"
	"gopkg.in/yaml.v1"
)

// TODO: implement publish

var Pkg = pkg{new(bool)}

type pkg struct {
	verbose *bool
}

func (p *pkg) SetVerbosity(v *bool) { p.verbose = v }

var OrigenDir string

func init() {
	var err error
	OrigenDir, err = os.Getwd()
	if err != nil {
		panic(err)
	}
}

// Build creates an app packaged.
// The package configuration must be named 'spec.yaml'.
//
// Returns the package path and the error, if any.
func (p pkg) Build(dir, system, arch string) (pkgPath string, err error) {
	workDir := dir
	if !filepath.IsAbs(dir) {
		workDir, err = filepath.Abs(dir)
		if err != nil {
			return "", err
		}
	}

	// Only want to compress files inside of the directory.
	if err = os.Chdir(workDir); err != nil {
		return
	}

	// == Read configuration

	configData, err := ioutil.ReadFile(filepath.Join(workDir, f_SPEC))
	if err != nil {
		return
	}

	var cfg Config
	if err = yaml.Unmarshal(configData, &cfg); err != nil {
		return
	}
	if err = cfg.Check(); err != nil {
		return
	}

	appName := NewAppName(cfg.Name, cfg.Version)

	pkgPath = fmt.Sprintf("%s%c%s",
		_ROOT_CACHE_ULOAD, os.PathSeparator, appName.Name(),
	)
	if err = checkExistence(pkgPath); err != nil {
		return
	}

	// == Checking of files

	var errList goutil.ErrorList
	errorCh := make(chan error)
	done := make(chan bool, 3)

	filesToZip, binToZip := make([]string, 0), make([]string, 0)
	filesCh, binCh := make(chan string), make(chan string)

	// Error handler
	go func() {
		for err := range errorCh {
			if err != nil {
				errList.Append(err)
			}
		}
		done <- true
	}()

	// Get files
	go func() {
		for f := range filesCh {
			filesToZip = append(filesToZip, f)
		}
		done <- true
	}()
	// The binaries go compressed in different files.
	go func() {
		for f := range binCh {
			binToZip = append(binToZip, f)
		}
		done <- true
	}()

	filepath.Walk(workDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			errorCh <- err
			return nil
		}
		// Skip directories and backup files.
		if info.IsDir() || (info.Mode().IsRegular() && path[len(path)-1] == '~') {
			return nil
		}

		relPath := path[len(workDir)+1:] // Relative path

		if filepath.Dir(relPath) == DIR_BIN {
			binCh <- relPath
		} else {
			filesCh <- relPath
		}
		return nil
	})

	close(filesCh)
	close(binCh)
	close(errorCh)
	<-done
	<-done
	<-done

	if len(errList) != 0 {
		return "", errList
	}

	appFiles := NewAppFiles(appName)

	if err = appFiles.checkZipFiles(filesToZip); err != nil {
		return "", err
	}

	defer func() {
		if err == nil && *p.verbose {
			goutil.Printf("App %q built in '%s'\n", appName.Name(), _ROOT_CACHE_ULOAD)
		}
	}()

	// == Compress

	zipFile, err := os.Create(pkgPath)
	if err != nil {
		return
	}
	defer func() {
		err2 := zipFile.Close()
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	wrZip := zip.NewWriter(zipFile)
	defer func() {
		err2 := wrZip.Close()
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	for _, f := range filesToZip {
		zfile, err := wrZip.Create(f)
		if err != nil {
			errList.Append(err)
			continue
		}
		data, err := ioutil.ReadFile(f)
		if err != nil {
			errList.Append(err)
			continue
		}
		if _, err = zfile.Write(data); err != nil {
			errList.Append(err)
			continue
		}
	}

	if len(errList) != 0 {
		return "", errList
	}
	return
}

// Clean erases the files in the cache.
func (p pkg) Clean() error {
	files, err := filepath.Glob(_ROOT_CACHE_DLOAD + string(os.PathSeparator) + "*")
	if err != nil {
		return err
	}

	var errList goutil.ErrorList
	for _, v := range files {
		if err = os.Remove(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// Create creates the skeleton of an app, adding all directories and the
// specification file.
//
// The version must follow semantic versioning (http://semver.org/)
func (p pkg) Create(name, version string) error {
	appName := NewAppName(name, version)
	err := appName.Checking()
	if err != nil {
		return err
	}

	UserCtx.once.Do(UserCtx.Init)

	rootDir := filepath.Join(DIR_PKG, name, version)
	if err = checkExistence(rootDir); err != nil {
		return err
	}

	appFiles := NewAppFiles(appName).AppendDir(rootDir)

	for _, v := range appFiles.dirs() {
		if err = os.MkdirAll(v, 0775); err != nil {
			return err
		}
		if err = os.Chown(v, UserCtx.UID, UserCtx.GID); err != nil {
			return err
		}
	}

	// Add files

	data := map[string]string{
		"Name":    name,
		"Version": version,
	}

	if err = fileFromTmpl(filepath.Join(rootDir, f_README), tmplReadme, data); err != nil {
		return err
	}
	if err = fileFromTmpl(filepath.Join(rootDir, f_CHANGELOG), tmplChangelog, data); err != nil {
		return err
	}
	if err = fileFromTmpl(filepath.Join(rootDir, f_SPEC), tmplSpec, data); err != nil {
		return err
	}

	if *p.verbose {
		goutil.Printf("App skeleton created into '%s'\n", rootDir)
	}
	return nil
}

// Get downloads and installs apps from the URL paths.
func (p pkg) Get(urlPath ...string) error {
	var errList goutil.ErrorList
	for _, v := range urlPath {
		if err := p._get(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// _get returns the body from urlPath's https resource.
func (p pkg) _get(urlPath string) error {
	/*if !strings.HasSuffix(urlPath, EXT_APP) {
		return nil, NoAppExtInURL
	}*/

	u, err := url.Parse("https://" + urlPath)
	if err != nil {
		return err
	}
	//u.RawQuery = "ver=" + ver // TODO: check pass version
	urlStr := u.String()

	resp, err := http.DefaultClient.Get(urlStr)
	if err != nil {
		return err
	}
	defer func() {
		if resp != nil {
			resp.Body.Close()
		}
	}()
	if *p.verbose {
		goutil.Printf("Fetching %s", urlStr)
	}
	if resp.StatusCode != 200 {
		return HTTPCodeError(resp.StatusCode)
	}

	// Create file
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = ioutil.WriteFile(urlPath, b, 0644); err != nil {
		return err
	}

	return p._install(urlPath)
}

// Install installs apps from the path.
func (p pkg) Install(path ...string) error {
	var errList goutil.ErrorList
	for _, v := range path {
		if err := p._install(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// _install installs an app.
func (p pkg) _install(path string) (err error) {
	if filepath.Ext(path) != EXT_APP {
		return ExtError(path)
	}

	zipRd, err := zip.OpenReader(path)
	if err != nil {
		return err
	}
	defer func() {
		if e := zipRd.Close(); e != nil && err == nil {
			err = e
		}
	}()

	var cfg Config
	configFound := false

	// Read the pkg configuration.
	for _, f := range zipRd.File {
		if f.Name == f_SPEC {
			rd, e := f.Open()
			if e != nil {
				err = e
				return
			}
			configData := make([]byte, f.UncompressedSize64)
			if _, err = io.ReadFull(rd, configData); err != nil {
				return
			}

			if err = yaml.Unmarshal(configData, &cfg); err != nil {
				return
			}
			if err = cfg.Check(); err != nil {
				return
			}

			configFound = true
			break
		}
	}
	if !configFound {
		return NoConfigError(path)
	}

	if err = os.Chdir(DIR_APP); err != nil {
		return
	}
	/*defer func() {
		e := os.Chdir(OrigenDir)
		if e != nil && err == nil {
			err = e
		}
	}()*/

	// Uncompress all files.
	for _, f := range zipRd.File {
		wr, e := os.Create(f.Name)
		if e != nil {
			err = e
			return
		}
		rd, e := f.Open()
		if e != nil {
			err = e
			return
		}

		if _, err = io.Copy(wr, rd); err != nil {
			return
		}
	}

	// Create the log file
	userFiles := NewUserFiles(NewAppName(cfg.Name, cfg.Version))

	file, err := os.OpenFile(userFiles.F_Log, os.O_CREATE, 0570)
	if err != nil {
		return err
	}
	return file.Close()
}

func (p pkg) _publish() error {
	return nil
}

// Purge removes all files installed from the apps.
func (p pkg) Purge(app ...string) error {
	var errList goutil.ErrorList
	for _, v := range app {
		if err := p._purge(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// _purge removes all files of an app.
func (p pkg) _purge(app string) error {
	appName, err := GetAppName(app)
	if err != nil {
		return err
	}

	appFiles := NewAppFiles(appName).AppendDir(ROOT_APP)
	if err = appFiles.Checking(); err != nil {
		return err
	}

	var errList goutil.ErrorList

	/*for _, v := range appFiles.filesInSubdirs() {
		if err = os.Remove(v); err != nil {
			errList.Append(err)
		}
	}*/
	for _, v := range appFiles.dirs() {
		if err = os.RemoveAll(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// Remove removes the apps installed.
//
// Note: All data related to the apps remains in their places, until that they
// are removed by the garbage collector.
func (p pkg) Remove(app ...string) error {
	var errList goutil.ErrorList
	for _, v := range app {
		if err := p._remove(v); err != nil {
			errList.Append(err)
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// _remove removes the app.
func (p pkg) _remove(app string) error {
	appName, err := GetAppName(app)
	if err != nil {
		return err
	}

	appFiles := NewAppFiles(appName)
	if err = appFiles.Checking(); err != nil {
		return err
	}

	// TODO: remove binaries, with info. via bin/ or from config.
	// Do benchmark to know which is faster to get info.
	return os.Remove(appFiles.root)
}

// Update does nothing.
// TODO: ?
func (p pkg) Update() error { return nil }

// Upgrade upgrades all apps on the system.
// TODO
func (p pkg) Upgrade() error { return nil }

// == Utility
//

func checkExistence(file string) error {
	_, err := os.Stat(file)
	if err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		if filepath.IsAbs(file) {
			file = filepath.Base(file)
		}
		return fmt.Errorf("%s: '%s'", os.ErrExist, file)
	}

	return nil
}

// == Errors

//var errFetch = errors.New("https fetch failed")

type HTTPCodeError int

func (e HTTPCodeError) Error() string {
	msg := "ignoring https fetch with status code "
	statusCode := http.StatusText(int(e))

	if statusCode != "" {
		return fmt.Sprintf("%s\"%s\"", msg, statusCode)
	} else {
		return msg + string(e)
	}
}

/*// NoAppExtInURL reports when the extension of an app is not found in the URL.
type NoAppExtInURL string

func (e NoAppExtInURL) Error() string {
	return "no app extension found in url: " + string(e)
}*/
