// Copyright 2014 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"fmt"
	//"runtime"
	"strings"
)

// osType represents an operating system.
type osType int

// Operating systems
const (
	_OS_LINUX osType = iota + 1
	_OS_FREEBSD
	_OS_DARWIN
	_OS_WINDOWS
)

func (t osType) String() string {
	switch t {
	case _OS_LINUX:
		return "linux"
	case _OS_FREEBSD:
		return "freebsd"
	case _OS_DARWIN:
		return "darwin"
	case _OS_WINDOWS:
		return "windows"
	}
	panic("unimplemented")
}

var validOS = map[string]osType{
	"linux":   _OS_LINUX,
	"freebsd": _OS_FREEBSD,
	"darwin":  _OS_DARWIN,
	"windows": _OS_WINDOWS,
}

// archType represents a computer architecture.
type archType int

// Architectures
const (
	_ARCH_386 archType = iota + 1
	_ARCH_AMD64
	_ARCH_ARM
)

func (t archType) String() string {
	switch t {
	case _ARCH_386:
		return "386"
	case _ARCH_AMD64:
		return "amd64"
	case _ARCH_ARM:
		return "arm"
	}
	panic("unimplemented")
}

var validArch = map[string]archType{
	"386":   _ARCH_386,
	"amd64": _ARCH_AMD64,
	"arm":   _ARCH_ARM,
}

// splitOSAndArch returns the operating system and architecture from a string
// joined by "-".
func splitOSAndArch(s string) (osType, archType, error) {
	res := strings.Split(s, "-")
	if len(res) != 2 {
		return 0, 0, fmt.Errorf(`the format for "os-arch" only must have one "-"`)
	}

	os, found := validOS[res[0]]
	if !found {
		return 0, 0, fmt.Errorf("unsopported operating system: %s", os)
	}

	arch, found := validArch[res[1]]
	if !found {
		return 0, 0, fmt.Errorf("unsopported architecture: %s", arch)
	}

	return os, arch, nil
}

// devLang represents a development language supported in AutoApp.
type devLang int

const (
	_LANG_GO devLang = iota + 1
	_LANG_RUST
)

func (l devLang) String() string {
	switch l {
	case _LANG_GO:
		return "Go"
	case _LANG_RUST:
		return "Rust"
	}
	panic("unimplemented")
}

// archsForOS represents the architectures supported for every operating system.
type archsForOS map[osType][]archType

// validGo represents the supported systems and architectures for language Go.
// http://golang.org/doc/install#requirements
var validGo = archsForOS{
	_OS_LINUX:   {_ARCH_386, _ARCH_AMD64, _ARCH_ARM},
	_OS_FREEBSD: {_ARCH_386, _ARCH_AMD64, _ARCH_ARM},
	_OS_DARWIN:  {_ARCH_386, _ARCH_AMD64},
	_OS_WINDOWS: {_ARCH_386, _ARCH_AMD64},
}

// validRust represents the supported systems and architectures for language Rust.
// http://www.rust-lang.org/install.html
var validRust = archsForOS{
	_OS_LINUX: {_ARCH_386, _ARCH_AMD64},
	//_OS_FREEBSD: {_ARCH_386, _ARCH_AMD64},
	_OS_DARWIN:  {_ARCH_386, _ARCH_AMD64},
	_OS_WINDOWS: {_ARCH_386},
}

// checkForLanguage checks both system and architecture for a development language.
func checkForLanguage(lang devLang, os osType, arch archType) error {
	var valid archsForOS
	switch lang {
	case _LANG_GO:
		valid = validGo
	case _LANG_RUST:
		valid = validRust
	}

	archs, _ := valid[os]
	for _, v := range archs {
		if v == arch {
			return nil
		}
	}
	return fmt.Errorf("architecture %s unsopported in system %s for language %s",
		arch.String(), os.String(), lang.String(),
	)
}
