// Copyright 2013 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"fmt"
	//"os"
	"path/filepath"
	"testing"

	"github.com/kless/goutil"
)

func TestCommand(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	cmdsInfo := []goutil.CommandInfo{
		/*{
			Args: fmt.Sprintf("-v create %s %s", APP_NAME, APP_VERSION),
			Out:  "",
		},*/
		{
			Args: fmt.Sprintf("-v build %s",
				filepath.Join("testdata", DIR_PKG, APP_NAME, APP_VERSION)),
			Out: "",
		},
	}

	/*name, err := Pkg.Build("app", "", "")
	if err != nil {
		t.Fatal(err)
	}

	println(name)
	/*if err = Pkg.Install(name); err != nil {
		t.Fatal(err)
	}*/

	err := goutil.TestCommand(filepath.Join("cmd", "autoapp"), cmdsInfo)
	if err != nil {
		t.Fatal(err)
	}
}
