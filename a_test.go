// Copyright 2014 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

const (
	APP_NAME    = "foo"
	APP_VERSION = "1.0.0"
	APP_OS      = "freebsd"
	APP_ARCH    = "arm"
)
