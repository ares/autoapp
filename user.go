// Copyright 2014 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import "sync"

// userContext represents the context for the caller user.
type userContext struct {
	once sync.Once

	name, home string
	UID, GID   int
}

var UserCtx userContext
