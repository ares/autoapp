// Copyright 2014 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import "testing"

func TestConfig(t *testing.T) {
	TEXT := "foo"

	var cfg = Config{
		Name:        APP_NAME,
		Version:     APP_VERSION,
		Description: TEXT,
		License:     "MIT",
		Category:    "GAME",
	}

	if err := cfg.Check(); err != nil {
		t.Error("unexpected error")
	}

	cfg.Name = ""
	if err := cfg.Check(); err == nil {
		t.Errorf("expected error")
	}
}

var authorTests = []struct {
	in string
	ok bool
}{
	{"Anton Suarez <foo@site.com>", true},
	{"Anton Suarez", true},

	{"Anton Suarez <site.com>", false},
	{"Anton Suarez site.com", false},
	{"Anton Suarez <.com>", false},
	{"Anton Suarez <foo@site.com", false},
	{"Anton Suarez foo@site.com>", false},
	{"Anton Suarez foo@site.com", false},

	{"Anton Suarez <foo@site>", false}, // no TLD
}

func TestValidateAuthor(t *testing.T) {
	posDomainItem := len(authorTests) - 1

	for i, tt := range authorTests {
		err := ValidateAuthor(tt.in)

		if tt.ok && err != nil {
			t.Errorf("%q => unexpected error", tt.in)
		} else if !tt.ok && err == nil {
			t.Errorf("%q => expected error", tt.in)
		}

		if i == posDomainItem {
			if _, ok := err.(domainError); !ok {
				t.Errorf("%q => expected error of type DomainError", tt.in)
			}
		}
	}
}

var pageTests = []struct {
	in string
	ok bool
}{
	{"http://site.com", true},
	{"http://www.site.com", true},

	{"www.site.com", false},
	{"site.com", false},

	{"http://sitecom", false}, // no TLD
}

func TestValidateHomepage(t *testing.T) {
	posPageItem := len(pageTests) - 1

	for i, tt := range pageTests {
		err := ValidateHomepage(tt.in)

		if tt.ok && err != nil {
			t.Errorf("%q => unexpected error", tt.in)
		} else if !tt.ok && err == nil {
			t.Errorf("%q => expected error", tt.in)
		}

		if i == posPageItem {
			if _, ok := err.(domainError); !ok {
				t.Errorf("%q => expected error of type DomainError", tt.in)
			}
		}
	}
}

var nameTests = []struct {
	in string
	ok bool
}{
	{"foo", true},
	{"foo_bar", true},
	{"foo12", true},
	{"_foo", true},

	{"0bar", false},
	{"bar-foo", false},
	{"bar.foo", false},
	{"bar foo", false},
	{"bár", false},
}

func TestValidateAppName(t *testing.T) {
	for _, tt := range nameTests {
		err := ValidateAppName(tt.in)

		if tt.ok && err != nil {
			t.Errorf("%q => unexpected error", tt.in)
		} else if !tt.ok && err == nil {
			t.Errorf("%q => expected error", tt.in)
		}
	}
}
