// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/kless/goutil"
)

const (
	DIR_APP   = "app"
	DIR_CACHE = "cache"
	DIR_LIB   = "lib"
	DIR_PKG   = "Apps" // or Packages
	DIR_SRV   = "srv"
	DIR_USERS = "Users"

	DIR_BIN    = "bin"  // Executables
	DIR_CONFIG = "conf" // Configuration files
	DIR_DATA   = "data" // Database, cache
	DIR_DOC    = "doc"  // Documentatation
	DIR_LOG    = "log"
	DIR_SHARE  = "share" // Static files to read by the app
	DIR_TOOL   = "tool"  // Scripts/executables to setup extra stuff
	DIR_WEB    = "web"   // User interface based in web
)

const (
	f_CHANGELOG = "CHANGELOG.md"
	f_README    = "README.md"
	f_SPEC      = "spec.yaml"
)

const (
	GROUP_USER = "appstore"
	EXT_APP    = ".aapp"
)

// The structure for the root directory of the apps is:
//
//  _ ROOT_APP
//  |
//  |___ DIR_BIN
//  |
//  |___ DIR_CACHE
//  |
//  |___ DIR_LIB
//  |
//  |___ DIR_PKG
//  |
//  |___ DIR_SRV
//  |
//  |___ DIR_USERS
//
const (
	ROOT_APP    = ROOT + DIR_APP
	_ROOT_BIN   = ROOT_APP + string(os.PathSeparator) + DIR_BIN
	_ROOT_CACHE = ROOT_APP + string(os.PathSeparator) + DIR_CACHE
	_ROOT_LIB   = ROOT_APP + string(os.PathSeparator) + DIR_LIB
	_ROOT_PKG   = ROOT_APP + string(os.PathSeparator) + DIR_PKG
	_ROOT_SRV   = ROOT_APP + string(os.PathSeparator) + DIR_SRV
	_ROOT_USERS = ROOT_APP + string(os.PathSeparator) + DIR_USERS

	_ROOT_CACHE_DLOAD = _ROOT_CACHE + string(os.PathSeparator) + "dload"
	_ROOT_CACHE_ULOAD = _ROOT_CACHE + string(os.PathSeparator) + "uload"

	_ROOT_LIB_GO = _ROOT_LIB + string(os.PathSeparator) + "go"
	//_ROOT_LIB_RUST = _ROOT_LIB + string(os.PathSeparator) + "rust"
)

// GetRootDirs returns the root directories for the apps.
func GetRootDirs() []string {
	return []string{
		ROOT_APP,
		_ROOT_BIN,
		_ROOT_CACHE,
		_ROOT_CACHE_DLOAD,
		_ROOT_CACHE_ULOAD,
		_ROOT_LIB,
		_ROOT_LIB_GO,
		//_ROOT_LIB_RUST,
		_ROOT_PKG,
		_ROOT_SRV,
		_ROOT_USERS,
	}
}

// UserFiles represents the directory structure of the user files for an app.
//
// The structure is:
//
//  _ ROOT_USER/[user name]/
//  |
//  |___ DIR_CONFIG/[app name]/[app version].cfg
//  |
//  |___ DIR_DATA/[app name]/[app version]/
//  |
//  |___ DIR_LOG/[app name]/[app version].log
//
type UserFiles struct {
	root string

	F_Config string
	D_Data   string
	F_Log    string
}

func NewUserFiles(appName *AppName) *UserFiles {
	userDir := filepath.Join(ROOT_APP, DIR_USERS, UserCtx.name)

	return &UserFiles{
		root: userDir,

		F_Config: filepath.Join(userDir, DIR_CONFIG, appName.name, appName.version) +
			".cfg",
		D_Data: filepath.Join(userDir, DIR_DATA, appName.name, appName.version),
		F_Log: filepath.Join(userDir, DIR_LOG, appName.name, appName.version) +
			".log",
	}
}

// AppFiles represents the directory structure for an app.
//
// The structure is:
//
//  _ ROOT_PKG/[name]/[version]/
//  |
//  |___ DIR_BIN
//  |
//  |___ DIR_CONFIG
//  |
//  |___ DIR_DATA
//  |
//  |___ DIR_DOC
//  |
//  |___ DIR_SHARE
//  |
//  |___ DIR_TOOL
//  |
//  |___ DIR_WEB
//
type AppFiles struct {
	root string

	// Files in root directory
	F_Changelog string
	F_Readme    string
	F_Spec      string
	//F_License      string
	//F_Patents      string
	//F_Authors      string
	//F_Contributors string

	// Directories
	D_Bin    string
	D_Config string
	D_Data   string
	D_Doc    string
	D_Share  string
	D_Tool   string
	D_Web    string

	// Files in subdirectories
	F_Config_ string
}

func NewAppFiles(appName *AppName) *AppFiles {
	return &AppFiles{
		root: filepath.Join(ROOT_APP, DIR_PKG, appName.name, appName.version),

		F_Changelog: f_CHANGELOG,
		F_Readme:    f_README,
		F_Spec:      f_SPEC,

		D_Bin:    DIR_BIN,
		D_Config: DIR_CONFIG,
		D_Data:   DIR_DATA,
		D_Doc:    DIR_DOC,
		D_Share:  DIR_SHARE,
		D_Tool:   DIR_TOOL,
		D_Web:    DIR_WEB,
	}
}

// AppendDir appends a directory in every path.
func (f *AppFiles) AppendDir(dir string) *AppFiles {
	dir += string(os.PathSeparator)

	f.F_Changelog = dir + f.F_Changelog
	f.F_Readme = dir + f.F_Readme
	f.F_Spec = dir + f.F_Spec

	f.D_Bin = dir + f.D_Bin
	f.D_Config = dir + f.D_Config
	f.D_Data = dir + f.D_Data
	f.D_Doc = dir + f.D_Doc
	f.D_Share = dir + f.D_Share
	f.D_Tool = dir + f.D_Tool
	f.D_Web = dir + f.D_Web

	return f
}

// binDirs returns the directories for binaries.
//func (f *AppFiles) binDirs() []string {
	
//}

// allFiles returns all regular files.
func (f *AppFiles) allFiles() []string {
	return []string{
		f.F_Changelog, f.F_Readme, f.F_Spec,
	}
}

// filesInRoot returns the files in root directory.
func (f *AppFiles) filesInRoot() []string {
	return []string{f.F_Changelog, f.F_Readme, f.F_Spec}
}

// dirs returns the directories.
func (f *AppFiles) dirs() []string {
	return []string{
		f.D_Bin, f.D_Config, f.D_Data, f.D_Doc, f.D_Share, f.D_Tool, f.D_Web,
	}
}

// filesRequired returns the files required for an app packaged.
func (f *AppFiles) filesRequired() map[string]struct{} {
	return map[string]struct{}{
		f.F_Changelog: struct{}{},
		f.F_Readme:    struct{}{},
		f.F_Spec:      struct{}{},
	}
}

// checkZipFiles checks the files that are going to be packaged.
func (f *AppFiles) checkZipFiles(files []string) error {
	dirs := f.dirs()
	//filesInRoot := f.filesInRoot()
	//filesInSubdirs := f.filesInSubdirs()

	//filesSkipped := make([]string, 0)
	filesRequired := f.filesRequired()

	// Directories and files that should not int the app.
	extraDirs := make([]string, 0)
	extraFiles := make([]string, 0)

	// TODO: check files like bin, log

	for _, f := range files {
		if strings.ContainsRune(f, os.PathSeparator) { // Check a directory
			dirBase := filepath.Dir(f)
			found := false

			for _, d := range dirs {
				if dirBase == d {
					found = true
					break
				}
			}
			if !found {
				extraDirs = append(extraDirs, f)
			}

		} else { // Check a root file
			_, found := filesRequired[f]
			if found {
				delete(filesRequired, f)
			} else {
				extraFiles = append(extraFiles, f)
			}
		}
	}

	var errList goutil.ErrorList

	if len(extraDirs) != 0 {
		dirs := make([]string, 0)
		plural := "directory"

		for _, d := range extraDirs {
			dirs = append(dirs, `'`+d+`'`)
		}
		if len(dirs) != 1 {
			plural = "directories"
		}

		errList.Append(fmt.Errorf("%s should not be in an app: %s",
			plural, strings.Join(dirs, ", ")),
		)
	}
	if len(extraFiles) != 0 {
		dirs := make([]string, 0)
		plural := ""

		for _, d := range extraDirs {
			dirs = append(dirs, `'`+d+`'`)
		}
		if len(dirs) != 1 {
			plural = "s"
		}

		errList.Append(fmt.Errorf("file%s should not be in an app: %s",
			plural, strings.Join(files, ", ")),
		)
	}

	if len(filesRequired) != 0 {
		files := make([]string, 0)
		plural := ""

		for k, _ := range filesRequired {
			files = append(files, `'`+k+`'`)
		}
		if len(files) != 1 {
			plural = "s"
		}

		errList.Append(fmt.Errorf("file%s required for app not found: %s",
			plural, strings.Join(files, ", ")),
		)
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// Checking checks that the files exist and correspond to the file type.
func (f *AppFiles) Checking() error {
	var errList goutil.ErrorList

	// Files
	for _, v := range f.allFiles() {
		if v != "" {
			fileInfo, err := os.Stat(v)
			if err != nil {
				errList.Append(err)
			} else if !fileInfo.Mode().IsRegular() {
				errList.Append(NoFileError(v))
			}
		}
	}

	// Directories
	for _, v := range f.dirs() {
		if v != "" {
			fileInfo, err := os.Stat(v)
			if err != nil {
				errList.Append(err)
			} else if !fileInfo.IsDir() {
				errList.Append(NoDirError(v))
			}
		}
	}

	if len(errList) != 0 {
		return errList
	}
	return nil
}

// == Templates
//

const (
	tmplReadme = "# {{.Name}}\n\n\n"

	tmplChangelog = `# Changelog

## Version {{.Version}}


`

	tmplSpec = `name: {{.Name}}
version: {{.Version}}
description: #<REQUIRED>
author: 
#authors:
homepage: 

category: #<REQUIRED>
game: 

license: #<REQUIRED>
#license_text: <For commercial license>

`
)

// fileFromTmpl renders the template to a file.
func fileFromTmpl(filename, tmpl string, data interface{}) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	if err = file.Chmod(0644); err != nil {
		return err
	}

	t := template.New(filename)
	if t, err = t.Parse(tmpl); err != nil {
		return err
	}
	return t.Execute(file, data)
}
