// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

// Installer is the interface to handle tasks related at installing and removing
// of apps.
type Installer interface {
	Install() error
	Remove() error
}
