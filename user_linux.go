// Copyright 2014 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"path/filepath"

	"github.com/kless/goutil"
	"github.com/kless/osutil/user"
)

// Init gets both UID and GID of the caller and are stored into a local context.
// Also, checks that the user is in the group GROUP_USER.
func (ctx userContext) Init() {
	g, err := user.LookupGroup(GROUP_USER)
	if err != nil {
		goutil.Fatalln(err)
	}

	u, err := user.LookupUser(user.GetUsername())
	if err != nil {
		goutil.Fatalln(err)
	}

	found := false
	for _, v := range user.Getgroups() {
		if v == g.GID {
			found = true
			break
		}
	}
	if !found {
		goutil.Fatalf("user must belongs to group %s\n", GROUP_USER)
	}

	UserCtx.UID, UserCtx.GID = u.UID, g.GID
	UserCtx.name, UserCtx.home = u.Name, filepath.Join(ROOT_APP, DIR_USERS, u.Name)
}
