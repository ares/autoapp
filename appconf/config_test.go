// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package appconf

import (
	"io/ioutil"
	"os"
	"testing"
)

type T struct {
	A string
	B struct {
		C int
		D []int
	}
}

type T1 struct {
	A string `config:"optional"`
	B T2
}

type T2 struct {
	C byte
	D []int
}

func TestConfig(t *testing.T) {
	var data1 T
	data2 := &T{
		"foo",
		struct {
			C int
			D []int
		}{
			3,
			[]int{1, 2},
		},
	}

	var data3 T1
	data4 := &T1{
		"bar",
		T2{33, []int{11, 21}},
	}

	// Create temporary directory
	dirPath, err := ioutil.TempDir("", "test-")
	if err != nil {
		t.Fatal(err)
	}
	if err = os.Chdir(dirPath); err != nil {
		goto _end
	}
	if err = os.Mkdir("config", 0750); err != nil {
		goto _end
	}

	// data1, data2
	if err = Save("data.yaml", data2); err != nil {
		goto _end
	}
	if err = Load(&data1, "data.yaml"); err != nil {
		goto _end
	}
	if data1.A != data2.A {
		t.Logf("got different data: data1.A: %q, data2.A: %q", data1.A, data2.A)
	}
	if data1.B.C != data2.B.C {
		t.Logf("got different data: data1.B.C: %q, data2.B.C: %q", data1.B.C, data2.B.C)
	}

	// data3, data4
	if err = Save("data.yaml", data4); err != nil {
		goto _end
	}
	if err = Load(&data3, "data.yaml"); err != nil {
		goto _end
	}
	if data3.A != data4.A {
		t.Logf("got different data: data3.A: %q, data4.A: %q", data3.A, data4.A)
	}
	if data3.B.C != data4.B.C {
		t.Logf("got different data: data3.B.C: %q, data4.B.C: %q", data3.B.C, data4.B.C)
	}

_end:
	if err != nil {
		t.Log(err)
	}
	if err = os.RemoveAll(dirPath); err != nil {
		t.Log(err)
	}
}
