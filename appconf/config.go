// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package appconf

import (
	"fmt"
	"io/ioutil"
	//"os"
	"path/filepath"
	"reflect"
	"strings"

	"launchpad.net/goyaml"
)

const (
	// The config folder contains the configuration options for the application.
	// This is independent from the config file "app.yaml", which is used by Auto App.
	DIR_CONFIG = "config"

	FILE_DEFAULT = "default.yaml"
)

// Load reads the configuration data from "filename" in "DIR_CONFIG" to be
// unmarshalled in "out".
// If "filename" is the empty string, it is used "FILE_DEFAULT".
//
// Maps, pointers to structs and ints, etc, may all be used as the "out" value.
func Load(out interface{}, filename string) error {
	/*wd, err := os.Getwd()
	if err != nil {
		return err
	}*/

	if filename == "" {
		filename = FILE_DEFAULT
	}

	data, err := ioutil.ReadFile(filepath.Join(/*wd,*/ DIR_CONFIG, filename))
	if err != nil {
		return err
	}

	return goyaml.Unmarshal(data, out)
}

// Save saves the configuration data from "in" to "filename" in "DIR_CONFIG".
//
// Maps, pointers to structs and ints, etc, may all be used as the "in" value.
func Save(filename string, in interface{}) error {
	data, err := goyaml.Marshal(in)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, data, 0644)
}

// Check checks that the configuration have all required fields.
// TODO: do it generic
func Check(in interface{}) error {
	structVal := reflect.ValueOf(in)
	structType := reflect.TypeOf(in)
	fieldErrors := make([]string, 0)

	for i := 0; i < structVal.NumField(); i++ {
		if structVal.Field(i).String() == "" {
			fieldName := structType.Field(i).Name
			if fieldName != "Subcategory" { // Optional field
				fieldErrors = append(fieldErrors, strings.ToLower(fieldName))
			}
		}
	}
	if len(fieldErrors) != 0 {
		return fmt.Errorf("some field is empty or does not exist:\n\t%v", fieldErrors)
	}
	return nil
}
