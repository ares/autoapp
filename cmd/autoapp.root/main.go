// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"flag"
	"os"
	"runtime"
	"syscall"

	"bitbucket.org/ares/autoapp"
	"github.com/kless/goutil"
	"github.com/kless/goutil/flagplus"
	"github.com/kless/osutil"
	"github.com/kless/osutil/user"
)

var (
	Add = flag.Bool("add", false, "add user")
	Del = flag.Bool("del", false, "remove user")

	Create = flag.Bool("create", false, "create apps directory")
	Remove = flag.Bool("remove", false, "remove apps directory")

	Verbose = flag.Bool("v", false, "verbose")
)

func main() {
	err := osutil.MustbeRoot()
	if err != nil {
		goutil.Fatalln(err)
	}

	commands := flagplus.NewCommand(
		"AutoApp is a multi-system package manager for static executables.",

		cmdSysUser.AddFlags("add", "del"),
		cmdDir.AddFlags("create", "remove"),
		cmdUser.AddFlags("add", "del"),
	)

	commands.AddGlobalFlags("v")
	commands.Parse()
}

var cmdSysUser = &flagplus.Subcommand{
	UsageLine: "sysuser <-add | -del>",
	Short:     "add/remove the system user and system group to handle apps",
	Long: `
"sysuser" adds or removes system user and group system with permission to handle apps.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) > 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		if *Add {
			GID, err := user.AddSystemGroup(autoapp.GROUP_USER)
			if err != nil {
				if user.IsExist(err) {
					g, err := user.LookupGroup(autoapp.GROUP_USER)
					if err != nil {
						goutil.Fatalln(err)
					}
					GID = g.GID
				} else {
					goutil.Fatalln(err)
				}
			} else if *Verbose {
				goutil.Printf("System group added: %s\n", autoapp.GROUP_USER)
			}

			_, err = user.AddSystemUser(autoapp.GROUP_USER, autoapp.ROOT_APP, GID)
			if err != nil {
				goutil.Fatalln(err)
			}
			if *Verbose {
				goutil.Printf("System user added: %s\n", autoapp.GROUP_USER)
			}

		} else if *Del {
			err := user.DelUser(autoapp.GROUP_USER)
			if err != nil {
				goutil.Error(err)
			} else if *Verbose {
				goutil.Printf("System user removed: %s\n", autoapp.GROUP_USER)
			}

			if err = user.DelGroup(autoapp.GROUP_USER); err != nil {
				goutil.Error(err)
			} else if *Verbose {
				goutil.Printf("System group removed: %s\n", autoapp.GROUP_USER)
			}

		} else {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
	},
}

var cmdUser = &flagplus.Subcommand{
	UsageLine: "user <-add | -del> <name>...",
	Short:     "add/remove users to the group that can handle apps",
	Long: `
"user" adds or removes users to the system group with permission to handle apps.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) == 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		if *Add {
			err := user.AddUsersToGroup(autoapp.GROUP_USER, args...)
			if err != nil {
				goutil.Fatalln(err)
			}
			if *Verbose {
				goutil.Printf("Add to group %q: %s\n", autoapp.GROUP_USER, args)
			}
		} else if *Del {
			err := user.DelUsersInGroup(autoapp.GROUP_USER, args...)
			if err != nil {
				goutil.Fatalln(err)
			}
			if *Verbose {
				goutil.Printf("Remove from group %q: %s\n", autoapp.GROUP_USER, args)
			}
		} else {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
	},
}

var cmdDir = &flagplus.Subcommand{
	UsageLine: "dir <-create | -remove>",
	Short:     "create/remove the apps directory",
	Long: `
"dir" creates or removes the directory structure used to store the apps.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) > 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		if *Create {
			_, err := os.Stat(autoapp.ROOT_APP)
			if !os.IsNotExist(err) {
				goutil.Fatalf("directory already exists: %q\n", autoapp.ROOT_APP)
			}

			g, err := user.LookupGroup(autoapp.GROUP_USER)
			if err != nil {
				goutil.Fatalln(err)
			}
			u, err := user.LookupUser(autoapp.GROUP_USER)
			if err != nil {
				goutil.Fatalln(err)
			}

			if err = os.Chdir(autoapp.ROOT); err != nil {
				goutil.Fatalln(err)
			}
			if runtime.GOOS != "windows" {
				syscall.Umask(0)
			}
			for _, v := range autoapp.GetRootDirs() {
				if err = os.Mkdir(v, 0775); err != nil {
					goutil.Fatalln(err)
				}
				if err = os.Chown(v, u.UID, g.GID); err != nil {
					goutil.Fatalln(err)
				}
			}

			if *Verbose {
				goutil.Printf("Directory structure created: %q\n", autoapp.ROOT_APP)
			}

		} else if *Remove {
			_, err := os.Stat(autoapp.ROOT_APP)
			if os.IsNotExist(err) {
				goutil.Fatalf("directory does not exist: %q\n", autoapp.ROOT_APP)
			}

			if err = os.RemoveAll(autoapp.ROOT_APP); err != nil {
				goutil.Fatalln(err)
			}
			if *Verbose {
				goutil.Printf("Directory structure removed: %q\n", autoapp.ROOT_APP)
			}

		} else {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
	},
}
