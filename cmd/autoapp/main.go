// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"flag"

	"bitbucket.org/ares/autoapp"
	"github.com/kless/goutil"
	"github.com/kless/goutil/flagplus"
)

var (
	System = flag.String("sys", "", "operating system")
	Arch   = flag.String("arch", "", "architecture")

	Verbose = flag.Bool("v", false, "verbose")
)

func main() {
	commands := flagplus.NewCommand(
		"AutoApp is a multi-system package manager for static executables.",

		cmdCreate,
		cmdBuild.AddFlags("sys", "arch"),
		//cmdPublish,

		cmdGet,
		cmdInstall,
		cmdRemove,
		cmdPurge,
		cmdUpdate,
		cmdUpgrade,
		cmdClean,
	)
	commands.AddGlobalFlags("v")
	commands.Parse()
}

// * * *

var cmdBuild = &flagplus.Subcommand{
	UsageLine: "build [-sys system -arch architecture] <app directory>",
	Short:     "create an app packaged",
	Long: `
"build" creates an app packaging all files in directory.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) != 1 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		autoapp.Pkg.SetVerbosity(Verbose)
		if _, err := autoapp.Pkg.Build(args[0], *System, *Arch); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdClean = &flagplus.Subcommand{
	UsageLine: "clean",
	Short:     "erase the cache",
	Long: `
"clean" erases the files in the cache.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if err := autoapp.Pkg.Clean(); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdCreate = &flagplus.Subcommand{
	UsageLine: "create <app name> <version>",
	Short:     "create the skeleton app",
	Long: `
"create" creates the skeleton app, adding all directories and the
specification file.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) != 2 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		autoapp.Pkg.SetVerbosity(Verbose)
		if err := autoapp.Pkg.Create(args[0], args[1]); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdGet = &flagplus.Subcommand{
	UsageLine: "get <app>...",
	Short:     "download apps",
	Long: `
"get" download and install the apps indicated in the arguments.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) == 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}

		autoapp.Pkg.SetVerbosity(Verbose)
		if err := autoapp.Pkg.Get(args...); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdInstall = &flagplus.Subcommand{
	UsageLine: "install <app>...",
	Short:     "install apps",
	Long: `
"install" installs the apps from the local system.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) == 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
		if err := autoapp.Pkg.Install(args...); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdPurge = &flagplus.Subcommand{
	UsageLine: "purge <app>...",
	Short:     "erase apps",
	Long: `
"purge" erases all files installed from apps.
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) == 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
		if err := autoapp.Pkg.Purge(args...); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdRemove = &flagplus.Subcommand{
	UsageLine: "remove <app>...",
	Short:     "remove apps",
	Long: `
"remove" moves the apps to the trash.

To recover an app from the trash, use the command "restore".
The trash collector will erase automatically the apps in the trash after a
few days.

Note: if you want to erase the app, use "purge".
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if len(args) == 0 {
			goutil.Fatalln("Usage: " + cmd.UsageLine)
		}
		if err := autoapp.Pkg.Remove(args...); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdUpdate = &flagplus.Subcommand{
	UsageLine: "update",
	Short:     "",
	Long: `
"update" .
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if err := autoapp.Pkg.Update(); err != nil {
			goutil.Fatalln(err)
		}
	},
}

var cmdUpgrade = &flagplus.Subcommand{
	UsageLine: "upgrade",
	Short:     "upgrade all apps",
	Long: `
"upgrade" upgrades all apps on the system..
`,
	Run: func(cmd *flagplus.Subcommand, args []string) {
		if err := autoapp.Pkg.Upgrade(); err != nil {
			goutil.Fatalln(err)
		}
	},
}
