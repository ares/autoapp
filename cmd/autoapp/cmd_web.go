// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build ignore

package main

import (
	"flag"
	"log"
	"net/http"
	"strconv"

	"code.google.com/p/go.net/websocket"
	"github.com/kless/goutil/flagplus"
)

// URLs of the web socket handlers.
const (
	WS_INSTALL = "/ws-install"
	WS_REMOVE  = "/ws-remove"
	WS_PURGE   = "/ws-purge"
)

var cmdWeb = &flagplus.Subcommand{
	Run:       runWeb,
	UsageLine: "web",
	Short:     "web service",
	Long: `
"web" starts the web service to handle the package manager from a browser.
`,
}

var (
	HTTP = flag.Int("http", 1180, "HTTP service address")
	WS   = flag.Int("ws", 1170, "web socket service address")
)

func init() {
	cmdWeb.AddFlags("http")
	cmdWeb.AddFlags("ws")
}

func runWeb(cmd *flagplus.Subcommand, args []string) {
	http.Handle(WS_INSTALL, websocket.Handler(InstallHandler))
	http.Handle(WS_REMOVE, websocket.Handler(RemoveHandler))
	http.Handle(WS_PURGE, websocket.Handler(PurgeHandler))

	err := http.ListenAndServe(":"+strconv.Itoa(*WS), nil)
	if err != nil {
		log.Fatal("ListenAndServe: " + err.Error())
	}
}

// == Handlers

func InstallHandler(ws *websocket.Conn) {
	var path string
	err := websocket.Message.Receive(ws, &path)
	if err != nil {
		log.Printf("can not receive source code: %s", err)
		return
	}
}

func RemoveHandler(ws *websocket.Conn) {

}

func PurgeHandler(ws *websocket.Conn) {

}
