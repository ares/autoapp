// Copyright 2013 The autoapp Authors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"os"
	"path/filepath"

	"github.com/kless/goutil"
)

var ROOT, DIR_HOME string

func init() {
	ROOT = os.Getenv("HOMEDRIVE") // C: or sometimes D:
	if ROOT == "" {
		goutil.Fatalln("environment variable is not set: HOMEDRIVE")
	}

	DIR_HOME = os.Getenv("HOMEPATH") // "\Users\{username}"
	if DIR_HOME == "" {
		goutil.Fatalln("environment variable is not set: HOMEPATH")
	}
	DIR_HOME += `\.` + DIR_APP
}

// _EXT_BIN is the extension for an executable binary.
const _EXT_BIN = ".exe"

// GetAppRoot returns the absolute path of apps directory.
//func GetAppRoot() string { return ROOT + `\\` + DIR_APP }
