// Copyright 2014 Jonas mg
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package autoapp

import (
	"fmt"
	"runtime"
	"testing"
)

func TestAppName(t *testing.T) {
	appName := NewAppName(APP_NAME, APP_VERSION)

	err := appName.Checking()
	if err != nil {
		t.Errorf("expected no error at checking, got %s", err)
	}

	if appName.name != APP_NAME {
		t.Errorf("name => got %q, want %q", appName.name, APP_NAME)
	}
	if appName.version != APP_VERSION {
		t.Errorf("version => got %q, want %q", appName.version, APP_VERSION)
	}
	if appName.os != runtime.GOOS {
		t.Errorf("os => got %q, want %q", appName.os, runtime.GOOS)
	}
	if appName.arch != runtime.GOARCH {
		t.Errorf("arch => got %q, want %q", appName.arch, runtime.GOARCH)
	}

	name_version := APP_NAME + "-" + APP_VERSION
	if appName.name_version != name_version {
		t.Errorf("name_version => got %q, want %q", appName.name_version, name_version)
	}

	fullName := fmt.Sprintf("%s-%s-%s-%s",
		APP_NAME, APP_VERSION, runtime.GOOS, runtime.GOARCH,
	)
	if appName.FullName() != fullName {
		t.Errorf("FullName(): got %q, want %q", appName.FullName(), fullName)
	}

	if err = appName.SetSystem(APP_OS, APP_ARCH); err != nil {
		t.Errorf("SetSystem(): unexpected error: %s", err)
	}
	if err = appName.SetSystem("darwi", APP_ARCH); err == nil {
		t.Errorf("SetSystem(): expected error with os %q", "darwi")
	}
	if err = appName.SetSystem(APP_OS, ""); err == nil {
		t.Errorf("SetSystem(): expected error with arch %q", "")
	}

	fullName = fmt.Sprintf("%s-%s-%s-%s", APP_NAME, APP_VERSION, APP_OS, APP_ARCH)
	if appName.FullName() != fullName {
		t.Errorf("FullName(): got %q, want %q", appName.FullName(), fullName)
	}
}

func TestGetAppName(t *testing.T) {
	fullName := fmt.Sprintf("%s-%s-%s-%s", APP_NAME, APP_VERSION, APP_OS, APP_ARCH)

	appName, err := GetAppName(fullName)
	if err != nil {
		t.Fatalf("GetAppName(): unexpected error: %s", err)
	}

	if appName.name != APP_NAME {
		t.Errorf("name => got %q, want %q", appName.name, APP_NAME)
	}
	if appName.version != APP_VERSION {
		t.Errorf("version => got %q, want %q", appName.version, APP_VERSION)
	}
	if appName.os != APP_OS {
		t.Errorf("os => got %q, want %q", appName.os, APP_OS)
	}
	if appName.arch != APP_ARCH {
		t.Errorf("arch => got %q, want %q", appName.arch, APP_ARCH)
	}

	name_version := APP_NAME + "-" + APP_VERSION
	if appName.name_version != name_version {
		t.Errorf("name_version => got %q, want %q", appName.name_version, name_version)
	}

	fullName = fmt.Sprintf("%s-%s-%s-%s", APP_NAME, APP_VERSION, APP_OS, APP_ARCH)
	if appName.FullName() != fullName {
		t.Errorf("FullName(): got %q, want %q", appName.FullName(), fullName)
	}
}
